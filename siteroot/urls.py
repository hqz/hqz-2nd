from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^hqz/', include('hqz.urls', namespace="hqz")),
                       # url(r'^chat/', include('chat.urls', namespace="chat")),
                       url('^$', include('hqz.urls', namespace="hqz")),
)
