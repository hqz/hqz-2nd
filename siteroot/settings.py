"""
Django settings for hqz project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'q=4#hiksc=zl_f(4^4^_5rerj+5=m5wue-7j8m4$kw168yy-f@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    # 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # 'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.admin',
    'hqzauth',
    'django.contrib.staticfiles',
    'conversejs',
    'hqz',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': '/var/www/weimeng1.hiquanzi.com/log/hqz.log',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'hqz': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'hqzauth': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'conversejs': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'siteroot': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

ROOT_URLCONF = 'siteroot.urls'

WSGI_APPLICATION = 'siteroot.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'ATOMIC_REQUESTS': True,
        'AUTOCOMMIT': True,
        'OPTIONS': {
            'read_default_file': '/opt/my_client.cnf',
            'init_command': 'SET storage_engine=INNODB',
        },
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'zh-cn'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

# Login URL
LOGIN_URL = '/'
LOGIN_REDIRECT_URL = '/hqz/main'
APPEND_SLASH = True

AUTH_USER_MODEL = 'hqzauth.AppUser'

# BOSH service URL
CONVERSEJS_BOSH_SERVICE_URL = 'http://hiquanzi.com/http-bind/'
# XMPP server
CONVERSEJS_AUTO_REGISTER = '121.199.56.232'
# Domain
CONVERSEJS_DOMAIN = 'hiquanzi.com'

