#!/usr/bin/python
# -*- coding: UTF-8 -*-
import datetime
from django.db import models


# Create your models here.
from hqzauth.models import AppUser


class Degree(models.Model):
    """
    学历。
    如：本科生，硕士生....
    """
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class School(models.Model):
    """
    学校
    """
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

class Activity(models.Model):
    '''
    活动
    '''
    name = models.CharField(max_length=100)
    describtion = models.CharField(max_length=400)
    start_time  = models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))
    address     = models.CharField(max_length=300)
    ptx   = models.CharField(max_length=200)
    pty   = models.CharField(max_length=200)
    chatrom_num = models.IntegerField(max_length=6, default=0)
    num_people_in_room = models.IntegerField()
class Chatrom(models.Model):
    owner = models.ForeignKey(Activity)
    id  = models.IntegerField(primary_key=True)

class Department(models.Model):
    """
    院系
    """
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Grade(models.Model):
    """
    年级
    """
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class Code(models.Model):
    """
    注册码
    """
    register_code = models.CharField(primary_key=True, max_length=50)
    creator = models.ForeignKey(AppUser)
    start_time = models.DateTimeField(auto_now_add=True)
    end_time = models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))
    type = models.CharField(max_length=20)
    times = models.IntegerField(max_length=6, default=0)

    def __unicode__(self):
        return self.register_code


class SchoolCoterie(models.Model):
    """
    学校圈子，以学校相关信息为单位的圈子
    """
    code = models.OneToOneField(Code, primary_key=True)
    school = models.ForeignKey(School)
    department = models.ForeignKey(Department)
    grade = models.ForeignKey(Grade)
    degree = models.ForeignKey(Degree)

    class Meta:
        db_table = 'hqz_school_coterie'

    def __unicode__(self):
        return self.school.name


class AppUserDetail(models.Model):
    """
    应用程序用户详细信息
    """
    app_user = models.OneToOneField(AppUser, primary_key=True)
    avatar = models.URLField(max_length=255, blank=True)
    school_coterie = models.ForeignKey(SchoolCoterie, blank=True)

    class Meta:
        db_table = 'hqz_app_user_detail'

    def __unicode__(self):
        return self.app_user.get_full_name()
