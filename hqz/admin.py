from django.contrib import admin

# Register your models here.
from hqz.models import AppUserDetail, Degree, School, Department, Grade, SchoolCoterie, Code,Activity
from hqzauth import AppUser
from hqzauth.admin import AppUserAdmin


class AppUserDetailInline(admin.StackedInline):
    model = AppUserDetail
    can_delete = False
    verbose_name_plural = 'user_detail'


class AppUserAdminExtend(AppUserAdmin):
    inlines = (AppUserDetailInline, )


admin.site.unregister(AppUser)
admin.site.register(AppUser, AppUserAdminExtend)


class DegreeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'short_name']}),
    ]
    list_display = ('name', 'short_name',)
    list_filter = ['short_name']
    search_fields = ['short_name']


class SchoolAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'short_name']}),
    ]
    list_display = ('name', 'short_name',)
    list_filter = ['short_name']
    search_fields = ['short_name']


class DepartmentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'short_name']}),
    ]
    list_display = ('name', 'short_name',)
    list_filter = ['short_name']
    search_fields = ['short_name']


class GradeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'short_name']}),
    ]
    list_display = ('name', 'short_name',)
    list_filter = ['short_name']
    search_fields = ['short_name']


class SchoolCoterieInline(admin.StackedInline):
    model = SchoolCoterie
    can_delete = False
    verbose_name_plural = 'School Coterie'


class CodeAdmin(admin.ModelAdmin):
    inlines = (SchoolCoterieInline, )


# class CodeInline(admin.StackedInline):
#     model = Code
#     can_delete = False
#     verbose_name_plural = 'Code'
#
#
# class SchoolCoterieAdmin(admin.ModelAdmin):
#     inlines = (CodeInline, )

class ActivityAdmin(admin.ModelAdmin):
    model = Activity
admin.site.register(Degree, DegreeAdmin)
admin.site.register(School, SchoolAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Grade, GradeAdmin)
admin.site.register(Code, CodeAdmin)
admin.site.register(Activity, ActivityAdmin)

