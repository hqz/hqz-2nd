(function (root, factory) {
    var translations = {
        "domain": "converse",
        "locale_data": {
            "converse": {
                "": {
                    "Project-Id-Version": "Converse.js 0.4",
                    "Report-Msgid-Bugs-To": "",
                    "POT-Creation-Date": "2013-12-18 22:06+0800",
                    "PO-Revision-Date": "2013-12-18 22:15+0800",
                    "Last-Translator": "Bill Lv<billcc.lv@gmail.com>",
                    "Language-Team": "German",
                    "Language": "de",
                    "MIME-Version": "1.0",
                    "Content-Type": "text/plain; charset=UTF-8",
                    "Content-Transfer-Encoding": "8bit",
                    "Plural-Forms": "nplurals=2; plural=(n != 1);"
                },
                "unencrypted": [
                    null,
                    "未加密"
                ],
                "unverified": [
                    null,
                    "未验证"
                ],
                "verified": [
                    null,
                    "已验证"
                ],
                "finished": [
                    null,
                    "完成"
                ],
                "Disconnected": [
                    null,
                    "断开连接"
                ],
                "Error": [
                    null,
                    "错误"
                ],
                "Connecting": [
                    null,
                    "连接中"
                ],
                "Connection Failed": [
                    null,
                    "连接失败"
                ],
                "Authenticating": [
                    null,
                    "验证中"
                ],
                "Authentication Failed": [
                    null,
                    "验证失败"
                ],
                "Disconnecting": [
                    null,
                    "断开连接中"
                ],
                "Re-establishing encrypted session": [
                    null,
                    "重新发布加密会话"
                ],
                "Your browser needs to generate a private key, which will be used in your encrypted chat session. This can take up to 30 seconds during which your browser might freeze and become unresponsive.": [
                    null,
                    ""
                ],
                "Private key generated.": [
                    null,
                    ""
                ],
                "Authentication request from %1$s\n\nYour buddy is attempting to verify your identity, by asking you the question below.\n\n%2$s": [
                    null,
                    ""
                ],
                "Could not verify this user's identify.": [
                    null,
                    ""
                ],
                "Personal message": [
                    null,
                    "个人信息"
                ],
                "Start encrypted conversation": [
                    null,
                    ""
                ],
                "Refresh encrypted conversation": [
                    null,
                    ""
                ],
                "End encrypted conversation": [
                    null,
                    ""
                ],
                "Verify with SMP": [
                    null,
                    ""
                ],
                "Verify with fingerprints": [
                    null,
                    ""
                ],
                "What's this?": [
                    null,
                    "这是什么?"
                ],
                "me": [
                    null,
                    "我"
                ],
                "Show this menu": [
                    null,
                    "显示此菜单"
                ],
                "Write in the third person": [
                    null,
                    "用第三个人写"
                ],
                "Remove messages": [
                    null,
                    "删除消息"
                ],
                "Your message could not be sent": [
                    null,
                    "你的消息无法发送"
                ],
                "We received an unencrypted message": [
                    null,
                    "我们收到一封未加密消息"
                ],
                "We received an unreadable encrypted message": [
                    null,
                    "我们收到一封不可读的加密消息"
                ],
                "This user has requested an encrypted session.": [
                    null,
                    ""
                ],
                "Here are the fingerprints, please confirm them with %1$s, outside of this chat.\n\nFingerprint for you, %2$s: %3$s\n\nFingerprint for %1$s: %4$s\n\nIf you have confirmed that the fingerprints match, click OK, otherwise click Cancel.": [
                    null,
                    ""
                ],
                "You will be prompted to provide a security question and then an answer to that question.\n\nYour buddy will then be prompted the same question and if they type the exact same answer (case sensitive), their identity will have been verified.": [
                    null,
                    ""
                ],
                "What is your security question?": [
                    null,
                    ""
                ],
                "What is the answer to the security question?": [
                    null,
                    ""
                ],
                "Invalid authentication scheme provided": [
                    null,
                    ""
                ],
                "Your messages are not encrypted anymore": [
                    null,
                    ""
                ],
                "Your messages are now encrypted but your buddy's identity has not been verified.": [
                    null,
                    ""
                ],
                "Your buddy's identify has been verified.": [
                    null,
                    ""
                ],
                "Your buddy has ended encryption on their end, you should do the same.": [
                    null,
                    ""
                ],
                "Your messages are not encrypted. Click here to enable OTR encryption.": [
                    null,
                    ""
                ],
                "Your messages are encrypted, but your buddy has not been verified.": [
                    null,
                    ""
                ],
                "Your messages are encrypted and your buddy verified.": [
                    null,
                    ""
                ],
                "Your buddy has closed their end of the private session, you should do the same": [
                    null,
                    ""
                ],
                "Contacts": [
                    null,
                    "联系人"
                ],
                "Online": [
                    null,
                    "在线"
                ],
                "Busy": [
                    null,
                    "忙碌"
                ],
                "Away": [
                    null,
                    "离开"
                ],
                "Offline": [
                    null,
                    "断线"
                ],
                "Click to add new chat contacts": [
                    null,
                    "点击添加新联系人"
                ],
                "Add a contact": [
                    null,
                    "添加一个联系人"
                ],
                "Contact username": [
                    null,
                    "联系人用户名"
                ],
                "Add": [
                    null,
                    "添加"
                ],
                "Contact name": [
                    null,
                    "联系人姓名"
                ],
                "Search": [
                    null,
                    "搜索"
                ],
                "No users found": [
                    null,
                    "找不到用户"
                ],
                "Click to add as a chat contact": [
                    null,
                    "点击添加一个聊天联系人"
                ],
                "Click to open this room": [
                    null,
                    "点击打开聊天室"
                ],
                "Show more information on this room": [
                    null,
                    "显示聊天室详情"
                ],
                "Description:": [
                    null,
                    "描述:"
                ],
                "Occupants:": [
                    null,
                    "占有者:"
                ],
                "Features:": [
                    null,
                    "特性:"
                ],
                "Requires authentication": [
                    null,
                    "需要验证"
                ],
                "Hidden": [
                    null,
                    "隐藏"
                ],
                "Requires an invitation": [
                    null,
                    "需要邀请"
                ],
                "Moderated": [
                    null,
                    "中等"
                ],
                "Non-anonymous": [
                    null,
                    "非匿名"
                ],
                "Open room": [
                    null,
                    "打开聊天室"
                ],
                "Permanent room": [
                    null,
                    "永久聊天室"
                ],
                "Public": [
                    null,
                    "公共"
                ],
                "Semi-anonymous": [
                    null,
                    "半匿名"
                ],
                "Temporary room": [
                    null,
                    "临时聊天室"
                ],
                "Unmoderated": [
                    null,
                    "未减速的"
                ],
                "Rooms": [
                    null,
                    "聊天室"
                ],
                "Room name": [
                    null,
                    "聊天室名"
                ],
                "Nickname": [
                    null,
                    "昵称"
                ],
                "Server": [
                    null,
                    "服务器"
                ],
                "Join": [
                    null,
                    "加入"
                ],
                "Show rooms": [
                    null,
                    "显示聊天室"
                ],
                "No rooms on %1$s": [
                    null,
                    "没有聊天室在 %1$s"
                ],
                "Rooms on %1$s": [
                    null,
                    "聊天室在 %1$s"
                ],
                "Set chatroom topic": [
                    null,
                    "设置聊天室主题"
                ],
                "Kick user from chatroom": [
                    null,
                    "把用户踢出"
                ],
                "Ban user from chatroom": [
                    null,
                    "禁止用户加入聊天室"
                ],
                "Message": [
                    null,
                    "消息"
                ],
                "Save": [
                    null,
                    "保存"
                ],
                "Cancel": [
                    null,
                    "放弃"
                ],
                "An error occurred while trying to save the form.": [
                    null,
                    "保存表单错误."
                ],
                "This chatroom requires a password": [
                    null,
                    "此聊天室需要密码"
                ],
                "Password: ": [
                    null,
                    "密码: "
                ],
                "Submit": [
                    null,
                    "提交"
                ],
                "This room is not anonymous": [
                    null,
                    "这个聊天室不是匿名的"
                ],
                "This room now shows unavailable members": [
                    null,
                    "This room now shows unavailable members"
                ],
                "This room does not show unavailable members": [
                    null,
                    "This room does not show unavailable members"
                ],
                "Non-privacy-related room configuration has changed": [
                    null,
                    "Non-privacy-related room configuration has changed"
                ],
                "Room logging is now enabled": [
                    null,
                    "Room logging is now enabled"
                ],
                "Room logging is now disabled": [
                    null,
                    "Room logging is now disabled"
                ],
                "This room is now non-anonymous": [
                    null,
                    "This room is now non-anonymous"
                ],
                "This room is now semi-anonymous": [
                    null,
                    "This room is now semi-anonymous"
                ],
                "This room is now fully-anonymous": [
                    null,
                    "This room is now fully-anonymous"
                ],
                "A new room has been created": [
                    null,
                    "A new room has been created"
                ],
                "Your nickname has been changed": [
                    null,
                    "Your nickname has been changed"
                ],
                "<strong>%1$s</strong> has been banned": [
                    null,
                    "<strong>%1$s</strong> has been banned"
                ],
                "<strong>%1$s</strong> has been kicked out": [
                    null,
                    "<strong>%1$s</strong> has been kicked out"
                ],
                "<strong>%1$s</strong> has been removed because of an affiliation change": [
                    null,
                    "<strong>%1$s</strong> has been removed because of an affiliation change"
                ],
                "<strong>%1$s</strong> has been removed for not being a member": [
                    null,
                    "<strong>%1$s</strong> has been removed for not being a member"
                ],
                "You have been banned from this room": [
                    null,
                    "You have been banned from this room"
                ],
                "You have been kicked from this room": [
                    null,
                    "You have been kicked from this room"
                ],
                "You have been removed from this room because of an affiliation change": [
                    null,
                    "You have been removed from this room because of an affiliation change"
                ],
                "You have been removed from this room because the room has changed to members-only and you're not a member": [
                    null,
                    "You have been removed from this room because the room has changed to members-only and you're not a member"
                ],
                "You have been removed from this room because the MUC (Multi-user chat) service is being shut down.": [
                    null,
                    "You have been removed from this room because the MUC (Multi-user chat) service is being shut down."
                ],
                "You are not on the member list of this room": [
                    null,
                    "You are not on the member list of this room"
                ],
                "No nickname was specified": [
                    null,
                    "No nickname was specified"
                ],
                "You are not allowed to create new rooms": [
                    null,
                    "You are not allowed to create new rooms"
                ],
                "Your nickname doesn't conform to this room's policies": [
                    null,
                    "Your nickname doesn't conform to this room's policies"
                ],
                "Your nickname is already taken": [
                    null,
                    "Your nickname is already taken"
                ],
                "This room does not (yet) exist": [
                    null,
                    "This room does not (yet) exist"
                ],
                "This room has reached it's maximum number of occupants": [
                    null,
                    "This room has reached it's maximum number of occupants"
                ],
                "Topic set by %1$s to: %2$s": [
                    null,
                    "Topic set by %1$s to: %2$s"
                ],
                "This user is a moderator": [
                    null,
                    "This user is a moderator"
                ],
                "This user can send messages in this room": [
                    null,
                    "This user can send messages in this room"
                ],
                "This user can NOT send messages in this room": [
                    null,
                    "This user can NOT send messages in this room"
                ],
                "Click to chat with this contact": [
                    null,
                    "Click to chat with this contact"
                ],
                "Click to remove this contact": [
                    null,
                    "Click to remove this contact"
                ],
                "This contact is busy": [
                    null,
                    ""
                ],
                "This contact is online": [
                    null,
                    ""
                ],
                "This contact is offline": [
                    null,
                    ""
                ],
                "This contact is unavailable": [
                    null,
                    ""
                ],
                "This contact is away for an extended period": [
                    null,
                    ""
                ],
                "This contact is away": [
                    null,
                    ""
                ],
                "Contact requests": [
                    null,
                    "Contact requests"
                ],
                "My contacts": [
                    null,
                    "My contacts"
                ],
                "Pending contacts": [
                    null,
                    "Pending contacts"
                ],
                "Custom status": [
                    null,
                    "Custom status"
                ],
                "Click to change your chat status": [
                    null,
                    "Click to change your chat status"
                ],
                "Click here to write a custom status message": [
                    null,
                    "Click here to write a custom status message"
                ],
                "online": [
                    null,
                    "online"
                ],
                "busy": [
                    null,
                    "busy"
                ],
                "away for long": [
                    null,
                    "away for long"
                ],
                "away": [
                    null,
                    "away"
                ],
                "I am %1$s": [
                    null,
                    "I am %1$s"
                ],
                "Sign in": [
                    null,
                    "Sign in"
                ],
                "XMPP/Jabber Username:": [
                    null,
                    "XMPP/Jabber 用户名:"
                ],
                "Password:": [
                    null,
                    "密码:"
                ],
                "Log In": [
                    null,
                    "登录"
                ],
                "BOSH Service URL:": [
                    null,
                    "BOSH Service URL:"
                ],
                "Online Contacts": [
                    null,
                    "Online Contacts"
                ],
                "Connected": [
                    null,
                    "已连接"
                ],
                "Attached": [
                    null,
                    "Attached"
                ]
            }
        }
    };
    if (typeof define === 'function' && define.amd) {
        define("de", ['jed'], function () {
            return factory(new Jed(translations));
        });
    } else {
        if (!window.locales) {
            window.locales = {};
        }
        window.locales.de = factory(new Jed(translations));
    }
}(this, function (i18n) {
    return i18n;
}));
