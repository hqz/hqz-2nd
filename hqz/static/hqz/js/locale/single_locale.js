/*
 * This file specifies a single language dependency (for Chinese).
 *
 * Translations take up a lot of space and you are therefore advised to remove
 * from here any languages that you don't need.
 */

(function (root, factory) {
    require.config({
        paths: {
            "jed": "Libraries/jed",
            "en": "locale/zh/LC_MESSAGES/zh"
        }
    });

    define("locales", [
        'jed',
        'zh'
    ], function (jed, zh) {
        root.locales = {};
        root.locales.zh = zh;
    });
})(this);
