#!/usr/bin/python
# -*- coding: UTF-8 -*-
__author__ = 'Bill Lyu'

MAIL_OCCUPIED = "邮箱: {0} 已被注册"
REGISTER_CODE_TIMES_INVALID = "注册码: {0} 超出使用次数"
REGISTER_CODE_NOT_EXIST = "注册码: {0} 不存在"
INTEGRITY_ERROR = "数据完整性错误"