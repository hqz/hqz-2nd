from django.conf.urls import patterns, url
from django.contrib import admin
from hqz import views

admin.autodiscover()
urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'hqz/index.html'},
                           name='login'),
                       url(r'^register/$', views.register, name='register'),
                       url(r'^(?P<pk>\d+)/loginreg/$', views.LoginView.as_view(), name='loginreg'),
                       url(r'^main/$', views.main, name='main'),
                       url(r'^sample/$', views.sample, name='sample'),
                       url(r'^sample2/$', views.sample2, name='sample2'),
)